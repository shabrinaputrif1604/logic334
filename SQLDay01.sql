--SQLDay01

--DDL (Data Definition Language)

--create database
CREATE DATABASE db_kampus;

USE db_kampus;

--drop database
DROP DATABASE db_kampus;

--create table
CREATE TABLE mahasiswa(
	id bigint PRIMARY KEY IDENTITY (1,1),
	name VARCHAR (50) NOT NULL,
	address VARCHAR (50) NOT NULL, 
	email VARCHAR (255) NULL
);

--create view
CREATE VIEW vw_mahasiswa
	AS SELECT * FROM mahasiswa;

SELECT * FROM vw_mahasiswa;

--alter
--add column
ALTER TABLE mahasiswa ADD
	phone_number1 VARCHAR(100) NOT NULL;

--drop column
ALTER TABLE mahasiswa DROP COLUMN phone_number;

--alter column
ALTER TABLE mahasiswa ALTER COLUMN 
	email VARCHAR (100) NOT NULL;	

--drop
--drop database 
--DROP DATABASE [nama_database]

--drop table
--DROP TABLE [nama_table]

--drop view
--DROP VIEW [nama_view]


--DML (Data Manipulation Language)

--select, insert, update, delete
--insert
INSERT INTO mahasiswa (name, address, email)
	VALUES 
	('Tunggul', 'Palembang', 'shabrinaputrif1604@gmail.com')
	--('Imam', 'Bekasi','imamassidqi@gmail.com'),
	--('Irvan', 'Jakarta', 'aliirvan122@gmail.com'),
	--('Rezky', 'Cengkareng', 'rezkyfajri08@gmail.com'),
	--('Tunggul', 'Semarang', 'tunggulyudhaputra5@gmail.com'),
	--('Shabrina', 'Palembang', 'shabrinaputrif1604@gmail.com')
	
;

--select
SELECT id, name, address, email 
	FROM dbo.mahasiswa;
SELECT * FROM dbo.mahasiswa;

--update
UPDATE mahasiswa SET name = 'Haikal Limansah' WHERE id = 1; 

--delete
DELETE mahasiswa WHERE name = 'tes';

--create table biodata
CREATE TABLE biodata(
	id BIGINT PRIMARY KEY IDENTITY(1,1),
	mahasiswa_id BIGINT NULL,
	tgl_lahir DATE NULL,
	gender VARCHAR (10) NULL
);

SELECT * from biodata;

INSERT INTO biodata (mahasiswa_id, tgl_lahir,gender)
	VALUES
	(1, '2002-06-10', 'PRIA'),
	(2, '2001-07-11', 'PRIA'),
	(3, '2000-08-12', 'WANITA')
;

--join (and, or, not) sama dengan inner join
SELECT mhs.id as id_mhs, mhs.name as nama_mhs, mhs.address as alamat_mhs, 
mhs.email as email_mhs, bio.tgl_lahir as tgl_lahir_mhs, 
bio.gender as gender_mhs
FROM mahasiswa as mhs
JOIN biodata as bio on mhs.id = bio.mahasiswa_id
WHERE mhs.id = 2 and mhs.name = 'imam' or not mhs.name = 'irvan' and mhs.id = 3;

--order by
SELECT * FROM biodata ORDER BY tgl_lahir DESC, gender ASC;

--top
SELECT TOP 3 * FROM biodata ORDER BY mahasiswa_id DESC;
SELECT TOP 1 mahasiswa_id FROM biodata ORDER BY mahasiswa_id DESC;

--between
SELECT * FROM biodata WHERE mahasiswa_id BETWEEN 2 AND 3;
SELECT * FROM biodata WHERE mahasiswa_id >= 2 AND mahasiswa_id <= 3;
SELECT * FROM biodata WHERE tgl_lahir BETWEEN '1999-01-01' AND '2001-01-01';

--like
SELECT * FROM mahasiswa 
	WHERE 
	--name LIKE 'i%'
	--name LIKE '%a'
	--name LIKE '%ai%'
	--name LIKE '_a%'
	--name LIKE 'i__%'
	name LIKE 'i%n'

--group by
SELECT * FROM mahasiswa;
SELECT name FROM mahasiswa GROUP BY name;

--aggregate function(sum, count, avg, min, max)
SELECT SUM(id), name, address FROM mahasiswa GROUP BY name, address;
SELECT SUM(id), name FROM mahasiswa GROUP BY name;

UPDATE mahasiswa SET address = 'Jogja' WHERE id = 8;

--having 
SELECT TOP 1 SUM(id) as jumlah, name
	FROM mahasiswa 
	GROUP BY name
	ORDER BY SUM(id) DESC

SELECT SUM(id) as jumlah, name
	FROM mahasiswa 
	GROUP BY name
	HAVING SUM(id) >= 13;

SELECT SUM(id) as jumlah, name
	FROM mahasiswa 
	WHERE name = 'Tunggul'
	GROUP BY name

SELECT SUM(id) as jumlah, name
	FROM mahasiswa 
	WHERE name = 'Tunggul'
	GROUP BY name
	HAVING SUM(id) >= 13
	ORDER BY name DESC;

--join