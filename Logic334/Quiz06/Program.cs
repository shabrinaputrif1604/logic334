﻿//Nomor1();
Nomor2();
//Nomor3();
//Nomor4();
//Nomor5();
//Nomor6();
//Nomor7();

//Test();


static void Nomor1()
{
	//Console.Write("Input n:\t");
	//int n = int.Parse(Console.ReadLine());

	//int x = 5;

	//for(int i = 0; i < n; i++)
	//{
	//	if (i % 2 == 0)
	//	{
	//		Console.Write($"-{x} ");
	//	}
	//	else
	//	{
	//		Console.Write($"{x} ");
	//	}

	//	x += 5;
	//}
}

static void Nomor2()
{
	Console.Write("Input waktu (mis. 02:02:01PM/02:02:01AM): ");
	//DateTime dt = DateTime.Parse(Console.ReadLine());
	//string time24 = dt.ToString("HH:mm:ss");

	//Console.WriteLine(time24);
}

static void Nomor3()
{
	Console.Write("Masukkan kode baju: ");
	int kodeBaju = int.Parse(Console.ReadLine());

	Console.Write("Masukkan kode ukuran: ");
	string kodeUkuran = Console.ReadLine().ToUpper();

	string merkBaju = "";
	int harga = 0;

  if (kodeBaju > 0 && kodeBaju < 4)
  {
		if (kodeBaju == 1)
		{
			merkBaju = "IMP";

			if (kodeUkuran == "S")
			{
				harga = 200000;
			}
			else if (kodeUkuran == "M")
			{
				harga = 220000;
			}
			else if(kodeUkuran != "L" || kodeUkuran != "XL")
			{
				Console.WriteLine("Ukuran tidak valid");
			}
			else
			{
				harga = 250000;
			}
		}
		else if (kodeBaju == 2)
		{
			merkBaju = "Prada";

			if (kodeUkuran == "S")
			{
				harga = 150000;
			}
			else if (kodeUkuran == "M")
			{
				harga = 160000;
			}
			else if (kodeUkuran != "L" || kodeUkuran != "XL")
			{
				Console.WriteLine("Ukuran tidak valid");
			}
			else
			{
				harga = 170000;
			}
		}
		else if (kodeBaju == 3)
		{
			if(kodeUkuran == "S" || kodeUkuran == "M" || kodeUkuran == "L" || kodeUkuran == "XL")
			{
				merkBaju = "Gucci";
				harga = 200000;
			}
			else
			{
				Console.WriteLine("Ukuran tidak valid");
			}
		}

		string currency = harga.ToString("C2");
		string currency2 = harga.ToString("#,#");
		string currency3 = harga.ToString("#,#.00");

		Console.WriteLine($"Merk baju : {merkBaju}");
	Console.WriteLine($"Harga : {currency3}");	
  }
	else
	{
		Console.Write("Baju tidak tersedia.");
	}
}

static void Nomor4()
{
	// pelajari dia kalo mau ditambahin dr index arr1 ke index arr2 gimana
	Console.Write("Uang Andi: ");
	int uang = int.Parse(Console.ReadLine());

	Console.Write("Harga Baju : ");
	string hargaBaju = Console.ReadLine();
	string[] splitBaju = hargaBaju.Split(',');
	int[] arrayBaju = Array.ConvertAll(splitBaju, int.Parse);


	Console.Write("Harga Celana : ");
	string hargaCelana = Console.ReadLine();
	string[] splitCelana = hargaCelana.Split(',');
	int[] arrayCelana = Array.ConvertAll(splitCelana, int.Parse);

	int terpilih = 0;

	if(arrayBaju.Length == arrayCelana.Length)
	{
		for (int i = 0; i < arrayBaju.Length; i++)
		{
			int jumlahBelanja = arrayBaju[i] + arrayCelana[i];

			if (jumlahBelanja <= uang && jumlahBelanja > terpilih)
			{
				terpilih = jumlahBelanja;
			}
		}
		Console.WriteLine($"Pakaian yang terpilih seharga {terpilih}");
	}
	else
	{
		Console.WriteLine("Panjang array tidak sama.");
	}
}

static void Nomor5()
{
	Console.Write("Input array (pemisah (,)) : ");
	string angka = Console.ReadLine();
	string[] valAngka = angka.Split(',');
	int[] arrayAngka = Array.ConvertAll(valAngka, int.Parse);

	Console.Write("Banyak rotasi : ");
	int rot = int.Parse(Console.ReadLine());
	
	for(int i = 0; i < rot; i++)
	{
		int temp = arrayAngka[0];
		for(int j = 0; j < arrayAngka.Length-1; j++)
		{
			arrayAngka[j] = arrayAngka[j + 1];
		}
		arrayAngka[arrayAngka.Length-1] = temp;
	}

	Console.Write(string.Join(", ", arrayAngka));
}

static void Nomor6()
{
	Console.Write("Input array : ");
	string n = Console.ReadLine();

	string[] nilaiArray = n.Split(',');

	int[] array = Array.ConvertAll(nilaiArray, int.Parse);

	int temp = 0;

	for (int i = 0; i < array.Length; i++)
	{
		for (int j = 0; j < array.Length - 1; j++)
		{
			if (array[j] > array[j + 1])
			{
				temp = array[j];
				array[j] = array[j + 1];
				array[j + 1] = temp;
			}
		}
	}

	Console.WriteLine(string.Join(", ", array));

}

static void Nomor7()
{
	Console.Write("n: ");
	int n = int.Parse(Console.ReadLine());

	//int x = 1;

	//for (int i = 0; i < n; i++)
	//{
	//	Console.Write($"{x}\t");
	//	x += 2;
	//}

	//Console.WriteLine();

	//x = 1;
	//for (int i = 0; i <= n; i++)
	//{
	//	Console.Write($"{x} ");
	//	x += 2;
	//}

	//Console.WriteLine();

	//x = 1;
	//for(int i = 0; i < n; i++)
	//{
	//	Console.Write($"{x} ");
	//	x += 3;
	//}

	//Console.WriteLine();

	//x = 1;
	//for (int i = 0; i < n; i++)
	//{
	//	Console.Write($"{x} ");
	//	x += 4;
	//}

	//Console.WriteLine();

	int x = 1;
	for (int i = 1; i <= n; i++)
	{
		if (i % 3 == 0)
		{
			Console.Write("*\t");
			continue;
		}
		
		Console.Write($"{x}\t");
		x += 4;
	}
}

static void Test()
{
	Console.Write("n: ");
	int n = int.Parse(Console.ReadLine());

	Console.Write("Masukkan nilai awal : ");
	int x = int.Parse(Console.ReadLine());

	Console.Write("Masukkan nilai tambah : ");
	int operasi = int.Parse(Console.ReadLine());

	for (int i = 0; i < n; i++)
	{
		Console.Write($"{x}\t");
		x += operasi;
	}

	Console.WriteLine();
}