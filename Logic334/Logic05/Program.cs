﻿//PadLeft();
RecursiveFunc();

static void PadLeft()
{
	Console.WriteLine("This is Pad Left and Pad Right.");

	Console.Write("Input: ");
	int input = int.Parse(Console.ReadLine());

	Console.Write("Insert length of char: ");
	int length = int.Parse(Console.ReadLine());

	Console.Write("Insert character: ");
	char chars = char.Parse(Console.ReadLine());

	//231100001 -> 2 year, 2 month, length

	DateTime date = DateTime.Now;

	string code = "";
	string code2 = "";

	code = date.ToString("yyMM") + input.ToString().PadLeft(length, chars);
	code2 = date.ToString("yyMM") + input.ToString().PadRight(length, chars);


	Console.WriteLine($"Hasil dari Pad Left adalah {code}");
	Console.WriteLine($"Hasil dari Pad Right adalah {code2}");
}

static void RecursiveFunc()
{
	Console.WriteLine("This is Recursive Function.");

	Console.Write("Input first: ");
	int start = int.Parse(Console.ReadLine());

	Console.Write("Input end: ");
	int end = int.Parse(Console.ReadLine());

	Console.Write("Masukkan tipe: (ASC/DESC)");
	string type = Console.ReadLine().ToUpper();

	Loop(start, end, type);
}

static int Loop(int start, int end, string type)
{

  if (type == "ASC")
  {
		if (start == end)
		{
			Console.WriteLine(end);
			return 0;
		}
		Console.WriteLine(start);
		return Loop(++start, end, type);
	}
	else
	{
		if (start == end)
		{
			Console.WriteLine(end);
			return 0;
		}
		Console.WriteLine(end);
		return Loop(start, --end, type);
	}
}

static void List2Dimensi()
{
	Console.WriteLine("--List 2 Dimensi--");
	List<List<int>> list = new List<List<int>>()
		{
				new List<int>() { 1, 2, 3 },
				new List<int>() { 4, 5, 6 },
				new List<int>() { 7, 8, 9 }
		};

	//tambah data
	list.Add(new List<int>() { 10, 11, 12 });

	//ubah data
	list[0][1] = 22; //ubah nilai 2 menjadi 22

	//hapus data
	list.RemoveAt(3); //hapus pakai index
										//list.Remove(list[3]); //hapus pakai value

	for (int i = 0; i < list.Count; i++)
	{
		for (int j = 0; j < list[i].Count; j++)
		{
			Console.Write(list[i][j] + "\t");
		}
		Console.WriteLine();
	}
}