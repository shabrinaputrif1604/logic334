﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic06
{
	public class Mobil
	{
		public double kecepatan;
		public double bensin;
		public double posisi;
		public string nama;
		public string platNo;

		// Ini constructor
		public Mobil(string _platNo) 
		{
			platNo = _platNo;
		}

		public Mobil()
		{

		}

		public string GetPlatNumber()
		{
			return platNo;
		}

		public void Utama()
		{
			Console.WriteLine($"Nama = {nama}");
			Console.WriteLine($"Plat nomor = {platNo}");

			Console.WriteLine($"Bensin = {bensin}");
			Console.WriteLine($"Kecepatan = {kecepatan}");
			Console.WriteLine($"Posisi = {posisi}");
		}

		public void Percepat()
		{
			this.kecepatan += 10;
			this.bensin -= 5;
		}

		public void Maju()
		{
			this.posisi += this.kecepatan;
			this.bensin -= 2;
		}

		public void IsiBensin(double bensin)
		{
			this.bensin += bensin;
		}
	}
}
