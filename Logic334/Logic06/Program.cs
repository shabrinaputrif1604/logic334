﻿using Logic06;

//AbstractClass();
//ObjectClass();
//Constructor();
//Encapsulation();
//Inheritance();
Overriding();


static void AbstractClass()
{
	Console.WriteLine("This is an abstract class");

	Console.Write("Input x: ");
	int x = int.Parse(Console.ReadLine());

	Console.Write("Input y: ");
	int y = int.Parse(Console.ReadLine());

	TestTurunan calc = new TestTurunan();

	int jumlah = calc.jumlah(x, y);
	Console.WriteLine($"Hasil {x} + {y} = {jumlah}");

	int kurang = calc.kurang(x, y);
	Console.WriteLine($"Hasil {x} - {y} = {kurang}");
}

static void ObjectClass()
{
	Console.WriteLine("This is an object class");

	Mobil mobil = new Mobil("") { nama = "Ferrari", kecepatan = 0, bensin = 10, posisi = 0 };

	mobil.Percepat();
	mobil.Maju();
	mobil.IsiBensin(20);

	mobil.Utama();
}

static void Constructor()
{
	Console.WriteLine("Constructor");

	Mobil mobil = new Mobil("BG 699 AR");

	string platNo = mobil.GetPlatNumber();

	Console.WriteLine($"Mobil dengan nomor polisi : {platNo}");
}

static void Encapsulation()
{
	Console.WriteLine("Encapsulation");

	PersegiPanjang pp = new PersegiPanjang();
	pp.panjang = 4.5;
	pp.lebar = 3.5;

	pp.TampilkanData();

}

static void Inheritance()
{
	Console.WriteLine("Inheritance");

	TypeMobil typeMobil = new TypeMobil();
	typeMobil.Civic();
}

static void Overriding()
{
	Console.WriteLine("Overriding");

	Kucing kucing = new Kucing();
	Paus paus = new Paus();

	Console.WriteLine($"Kucing {kucing.pindah()}");
	Console.WriteLine($"Paus {paus.pindah()}");

}