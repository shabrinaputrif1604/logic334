﻿//IfStatement();
//IfElseStatement();
//ElseIfStatement();
//NestedIf();	
//Ternary();
SwitchCase();

static void IfStatement()
{
	Console.WriteLine("This is a If Statement method");

	Console.Write("Input x: ");
	int x = int.Parse(Console.ReadLine());

	Console.Write("Input y: ");
	int y = int.Parse(Console.ReadLine());

	if(x >= 10)
	{
		Console.WriteLine("X is greater than or equals to 10.");
	}

	if(y <= 5)
	{
		Console.WriteLine("Y is less than or equals to 5.");
	}
}

static void IfElseStatement()
{
	Console.WriteLine("This is a If Else Statement method");

	Console.Write("Input x: ");
	int x = int.Parse(Console.ReadLine());

  if (x >= 10)
  {
		Console.WriteLine("X is greater than or equals to 10.");
	}
	else
	{
		Console.WriteLine("X is less than 10.");
	}
}

static void ElseIfStatement()
{
	Console.WriteLine("This is a Else If Statement method");

	Console.Write("Input x: ");
	int x = int.Parse(Console.ReadLine());

  if (x == 10)
  {
		Console.WriteLine("X is equals to 10.");
  }else if(x > 10)
	{
		Console.WriteLine("X is greater than 10.");
	}
	else
	{
		Console.WriteLine("X is less than 10");
	}
}

static void NestedIf()
{
	Console.WriteLine("This is a Nested If Statement method");

	Console.Write("Input your score: ");
	int x = int.Parse(Console.ReadLine());

	if(x >= 50)
	{
		Console.WriteLine("Kamu berhasil!");
    if (x == 100)
    {
			Console.WriteLine("Kamu keren!");
    }
	}
	else
	{
		Console.WriteLine("Kamu gagal.");
	}
}

static void Ternary()
{
	Console.WriteLine("This is a Ternary Statement method");

	Console.Write("Input x: ");
	int x = int.Parse(Console.ReadLine());

	Console.Write("Input y: ");
	int y = int.Parse(Console.ReadLine());

  if (x > y)
  {
		Console.WriteLine("X is greater than Y.");
	}
	else if(x < y)
	{
		Console.WriteLine("X is less than Y.");
	}
	else
	{
		Console.WriteLine("X is equals to Y.");
	}

	// Ternary
	string ternaryElseIf = (x > y) ? "X is greater than Y." : (x < y) ? "X is less than Y." : "X is equals to Y.";
	Console.WriteLine(ternaryElseIf);
}

static void SwitchCase()
{
	Console.Write("Giveaway samrtphone. Insert a number: ");
	int input = int.Parse(Console.ReadLine());

	switch (input)
	{
		case 1:
			Console.WriteLine("Congratulations! It's a Blackberry!");
			break;
		case 2:
			Console.WriteLine("Congratulations! It's a Nokia!");
			break;
		case 3:
			Console.WriteLine("Congratulations! It's a Xiaomi!");
			break;
		default:
			Console.WriteLine("Try again later...");
			break;
	}
}