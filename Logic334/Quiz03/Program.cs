﻿//Console.WriteLine($"Grade {Nomor1()}!");
//Console.WriteLine($"Point: \t {Nomor2()}");
//Nomor3();
//Nomor4();
//Nomor5();
//Nomor6();
Nomor7();
//Console.WriteLine(Nomor8());


static string Nomor1()
{
	Console.Write("Masukkan nilaimu: ");
	int score = int.Parse(Console.ReadLine());

	if(score > 0 && score <= 100)
	{
		if(score >= 90)
		{
			return "A";
		}
		else if(score >= 70 && score < 90)
		{
			return "B";
		}
		else if (score >= 50 && score < 69)
		{
			return "C";
		}
		else if (score < 50)
		{
			return "D";
		}
	}

	return "tidak valid";
}

static int Nomor2()
{
	Console.Write("Pulsa: \t ");
	int pulsa = int.Parse(Console.ReadLine());

	int point = 0;

	if(pulsa >= 10000 && pulsa < 25000)
	{
		return point += 80;
	}
	else if(pulsa >= 25000 && pulsa < 50000)
	{
		return point += 200;
	}
	else if (pulsa >= 50000 && pulsa < 100000)
	{
		return point += 400;
	}
	else if (pulsa == 100000)
	{
		return point += 800;
	}
	else
	{
		return 0;
	}
}

static void Nomor3()
{
	Console.Write("Belanja: \t ");
	int shop = int.Parse(Console.ReadLine());

	Console.Write("Jarak: \t ");
	int dist = int.Parse(Console.ReadLine());

	Console.Write("Masukkan Promo: \t ");
	string promo = Console.ReadLine().ToUpper();

	int discount, ongkir, shopTotal = 0;

	if (dist <= 5)
	{
		ongkir = 5000;
	}
	else
	{
		dist -= 5;
		ongkir = (dist * 1000) + 5000;
	}

	if (promo == "JKTOVO")
	{
		

		if(shop >= 30000)
		{
			discount = shop * 40 / 100;

			if(discount > 30000)
			{
				discount = 30000;
			}
		}
		else
		{
			discount = 0;
		}

		shopTotal = shop - discount + ongkir;

		Console.WriteLine($"Belanja: \t {shop}");
		Console.WriteLine($"Diskon 40%: \t {discount}");
		Console.WriteLine($"Ongkir: \t {ongkir}");
		Console.WriteLine($"Belanja: \t {shopTotal}");
	}
	else
	{
		shopTotal = shop + ongkir;

		Console.WriteLine($"Belanja: \t {shop}");
		Console.WriteLine($"Diskon 40%: \t 0");
		Console.WriteLine($"Ongkir: \t {ongkir}");
		Console.WriteLine($"Belanja: \t {shopTotal}");
	}

}

static void Nomor4()
{
	Console.Write("Belanja: \t");
	int shop = int.Parse(Console.ReadLine());

	Console.Write("Ongkos kirim: \t");
	int deliveryFee = int.Parse(Console.ReadLine());

	Console.Write("Pilih voucher: \t");
	int voucher = int.Parse(Console.ReadLine());

	int discountShop = 0, discountDelivery = 0;

	Console.WriteLine($"Belanja: \t {shop}");
	Console.WriteLine($"Ongkos kirim: \t {deliveryFee}");

  if (shop < 30000)
  {
		 switch (voucher)
		{
			case 1:
				if(shop >= 30000)
				{
					discountShop = 5000;
					shop -= discountShop;
				}
				break;

			case 2:
				if (shop >= 50000)
				{
					discountShop = 10000;
					shop -= discountShop;
				}
				break;

			case 3:
				if (shop >= 100000)
				{
					discountShop = 10000;
					shop -= discountShop;
				}
				break;
		}

		if (deliveryFee >= 5000)
		{
			discountDelivery = 5000;
		}
		else if (deliveryFee >= 10000)
		{
			discountDelivery = 10000;
		}

		deliveryFee -= discountDelivery;

		int total = shop + deliveryFee;
		Console.WriteLine($"Diskon ongkir: \t {discountDelivery}");
		Console.WriteLine($"Diskon belanja:\t{discountShop}");
		Console.WriteLine($"Total belanja: \t {total}");
  }
	else
	{
		Console.WriteLine("Voucher tidak valid.");
	}
}

static string GenClassification(int birthYear)
{
	if (birthYear >= 1944 && birthYear <= 1964)
	{
		return "Baby Boomer";
	}
	else if (birthYear >= 1965 && birthYear <= 1979)
	{
		return "Generasi X";
	}
	else if (birthYear >= 1980 && birthYear <= 1994)
	{
		return "Generasi Y";
	}
	else if (birthYear >= 1995 && birthYear <= 2015)
	{
		return "Generasi Z";
	}
	else
	{
		return "Belum tergolong kemana pun.";
	}

}

static void Nomor5()
{
	Console.Write("Masukkan nama Anda: ");
	string name = Console.ReadLine();

	Console.Write("Tahun berapa Anda lahir: ");
	int birthYear = int.Parse(Console.ReadLine());

  if (birthYear >= 1944 && birthYear <= 2015)
  {
		Console.WriteLine($"{name}, berdasarkan tahun lahir, Anda tergolong {GenClassification(birthYear)}");
	}
	else
	{
		Console.WriteLine(GenClassification(birthYear));
	}
}

static void Nomor6()
{
	Console.Write("Nama: ");
	string nama = Console.ReadLine();

	Console.Write("Tunjangan: ");
	int tunjangan = int.Parse(Console.ReadLine());

	Console.Write("Gapok: ");
	int gapok = int.Parse(Console.ReadLine());

	Console.Write("Banyak Bulan: ");
	int bulan = int.Parse(Console.ReadLine());

	int gajiKotor = gapok + tunjangan;
	int pajak = 0;

	if(gajiKotor <= 5000000)
	{
		pajak += gajiKotor * 5/100;
	}
	else if (gajiKotor > 5000000 && gajiKotor <= 10000000)
	{
		pajak += gajiKotor * 10 /100;
	}
	else
	{
		pajak += gajiKotor * 15 /100;
	}

	int bpjs = gajiKotor * 3 / 100;
	int gajiBersih = gajiKotor - (pajak + bpjs);
	int totalBulanGaji = gajiBersih * bulan;

	Console.WriteLine($"Karyawan atas nama {nama} slip gaji sebagai berikut: ");
	Console.WriteLine($"Pajak: \t\t\t\t Rp{pajak}");
	Console.WriteLine($"BPJS: \t\t\t\t Rp{bpjs}");
	Console.WriteLine($"Gaji/bulan: \t\t\t\t Rp{gajiBersih}");
	Console.WriteLine($"Total gaji/banyak bulan: \t\t\t\t Rp{totalBulanGaji}");
}

static void Nomor7()
{
	Console.Write("Masukkan berat badan Anda (kg): ");
	int weight = int.Parse(Console.ReadLine());

	Console.Write("Masukkan tinggi badan Anda (cm): ");
	int height = int.Parse(Console.ReadLine());

	double heightInM = Convert.ToDouble(height) / 100;
	double bmiVal = weight / (Math.Pow(heightInM, 2));

	Console.WriteLine($"Nilai BMI Anda adalah {Math.Round(bmiVal, 4)}");

	if (bmiVal < 18.5)
	{
		Console.WriteLine("Anda termasuk berbadan terlalu kurus.");
	}
	else if (bmiVal >= 18.5 && bmiVal < 25)
	{
		Console.WriteLine("Anda termasuk berbadan langsing.");
	}
	else
	{
		Console.WriteLine("Anda termasuk berbadan gemuk.");
	}
}

static string Nomor8()
{
	Console.Write("Masukkan nilai MTK: ");
	int mathScore = int.Parse(Console.ReadLine());

	Console.Write("Masukkan nilai Fisika: ");
	int physicsScore = int.Parse(Console.ReadLine());

	Console.Write("Masukkan nilai Kimia: ");
	int chemsScore = int.Parse(Console.ReadLine());

	int avg = (mathScore + physicsScore + chemsScore)/3;
	Console.WriteLine(avg);

	if(avg > 50)
	{
		return "Selamat\nKamu Keren\nKamu Hebat!";
	}
	else
	{
		return "Maaf\nKamu Gagal.";
	}
}