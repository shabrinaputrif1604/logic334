﻿// Call the method here
//Convertion();
//PlusOperator();
//ModOperator
// AssignmentOperator();
//ComparisonOperator();
//LogicalOperator();
//Result(5, 6);
MethodReturnType();

// Method
static void Convertion()
{
	Console.WriteLine("This is Convertion Method");

	int myInt = Convert.ToInt32(Console.ReadLine());
	double myDouble = Convert.ToDouble(Console.ReadLine());
	bool myBool = false;

	string strmyInt = Convert.ToString(myInt);
	string strmyInt2 = myInt.ToString();

	Console.WriteLine(strmyInt);
	Console.WriteLine(strmyInt2);
	Console.WriteLine(myDouble.ToString());
	Console.WriteLine(Convert.ToDouble(myInt));
	Console.WriteLine(myBool.ToString());
}

static void PlusOperator()
{
	int mangga, apel, hasil = 0;

	Console.WriteLine("This is Aritmatic Operator Method");

	Console.Write("Jumlah mangga: ");
	mangga = int.Parse(Console.ReadLine());

	Console.Write("Jumlah apel: ");
	apel = int.Parse(Console.ReadLine());

	hasil = mangga + apel;
	Console.WriteLine($"Jumlah mangga dan apel yang ada adalah {hasil}");
}

static void ModOperator()
{
	int mangga, orang, hasil = 0;

	Console.WriteLine("This is Modulus Operator Method");

	Console.Write("Jumlah mangga: ");
	mangga = int.Parse(Console.ReadLine());

	Console.Write("Jumlah orang: ");
	orang = int.Parse(Console.ReadLine());

	hasil = mangga % orang;
	Console.WriteLine($"Sisa mangga yang dibagikan ke orang-orang adalah {hasil}");
}

static void AssignmentOperator()
{
	int mangga = 10;
	int apel = 8;

	// replace variable
	mangga = 15;

	Console.WriteLine("This is Assignment Operator Method");

	// replace variable using scanner
	Console.Write("Masukkan mangga: ");
	mangga = int.Parse(Console.ReadLine());
	Console.WriteLine("Mangga: " + mangga);

	Console.Write("Masukkan apel: ");
	apel = int.Parse(Console.ReadLine());
	Console.WriteLine("Apel: " + apel);

	apel += 10;

	Console.WriteLine($"Mangganya ada {mangga} dan apelnya ada {apel}");
}

static void ComparisonOperator()
{
	int mangga, apel = 0;

	Console.WriteLine("This is Comparison Operator Method");

	Console.Write("Masukkan mangga: ");
	mangga = int.Parse(Convert.ToString(Console.ReadLine()));
	Console.Write("Masukkan apel: ");
	apel = int.Parse(Convert.ToString(Console.ReadLine()));

	Console.WriteLine("Hasil perbandingan: ");
	Console.WriteLine($"Mangga > Apel = {mangga > apel}");
	Console.WriteLine($"Mangga < Apel = {mangga < apel}");
	Console.WriteLine($"Mangga >= Apel = {mangga >= apel}");
	Console.WriteLine($"Mangga >= Apel = {mangga >= apel}");
	Console.WriteLine($"Mangga = Apel = {mangga == apel}");
	Console.WriteLine($"Mangga != Apel = {mangga != apel}");
}

static void LogicalOperator()
{
	Console.WriteLine("This is Logical Operator Method");

	Console.Write("Enter your age: ");
	int age = int.Parse(Console.ReadLine());

	Console.Write("Enter the password: ");
	string password = Console.ReadLine();

	bool isAdult = age > 18;
	bool isPasswordValid = password == "admin";

	if(isAdult && isPasswordValid)
	{
		Console.WriteLine("Welcome to the jungle!");
	}
	else
	{
		Console.WriteLine("Sorry, write again.");
	}
}

static int Result(int mango, int apple)
{
	int result = mango + apple;

	return result;
}

static void MethodReturnType()
{
	Console.WriteLine("This is a Method to Print Return Type Method");

	Console.Write("Insert mango: ");
	int mango = int.Parse(Console.ReadLine());

	Console.Write("Insert apple: ");
	int apple = int.Parse(Console.ReadLine());

	int result = Result(mango, apple);

	Console.WriteLine($"The result is {result}");
}