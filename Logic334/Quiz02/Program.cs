﻿//Tugas4();
Console.WriteLine($"Grade {Tugas5()}!");
//Tugas6();

static void Tugas4()
{
	Console.Write("Puntung yang didapatkan hari ini: ");
	int puntung = int.Parse(Console.ReadLine());

	// Puntung yang dapat dihasilkan
	int rokok = puntung / 8;
	int sisaPuntung = puntung % 8;

	// Hasil jual
	int hargaJual = 500;
	int hasilJual = hargaJual * rokok;

	if(rokok > 0)
	{
		Console.WriteLine($"Hari ini dapat {rokok} batang rokok.");
		Console.WriteLine($"Sisa puntungnya ada {sisaPuntung}.");
	}



	Console.WriteLine($"Hasil jual {rokok} batang rokok adalah Rp{hasilJual}");
}

static string Tugas5()
{
	Console.Write("Input skor kamu: ");
	int skor = int.Parse(Console.ReadLine());

	if(skor > 0 && skor <= 100)
	{
		if (skor >= 80)
		{
			return "A";
		}
		else if(skor >= 60 && skor < 80)
		{
			return "B";
		}
		else if(skor < 60)
		{
			return "C";
		}
	}

	return "tidak valid";
}

static void Tugas6()
{
	Console.WriteLine("Masukkan sebuah bilangan: ");
	int n = int.Parse(Console.ReadLine());

	Console.WriteLine(n % 2 == 0 ? $"{n} adalah bilangan genap." : $"{n} adalah bilangan ganjil.");
}