﻿Console.WriteLine(Tugas1());
//Console.WriteLine(Tugas2());
//Console.WriteLine(Tugas3());


static string Tugas1()
{
	Console.WriteLine("Mencari Luas dan Keliling Lingkaran.");

	Console.Write("Input jari-jari: ");
	int r = int.Parse(Console.ReadLine());

	double keliling = 2 * Math.PI * r;
	double luas = Math.PI * r * r;

	//return $"Jika jari-jari adalah {r}, maka kelilingnya adalah {keliling} dan luasnya adalah {luas}";
	// Math.Round untuk pembulatan (2 angka di belakang koma)
	return $"Jika jari-jari adalah {r}, maka kelilingnya adalah {Math.Round(keliling, 2, MidpointRounding.AwayFromZero)} dan luasnya adalah {Math.Round(luas, 2, MidpointRounding.ToZero)}";
}

static string Tugas2()
{
	Console.WriteLine("Mencari Luas dan Keliling Persegi.");

	Console.Write("Input panjang sisi: ");
	int s = int.Parse(Console.ReadLine());

	int luas = s * s;
	int keliling = 4 * s;

	return $"Jika panjang sisinya adalah {s}, maka kelilingnya adalah {keliling} dan luasnya adalah {luas}";
}

static string Tugas3()
{
	Console.WriteLine("Modulus");

	Console.Write("Input angka: ");
	int angka = int.Parse(Console.ReadLine());

	Console.Write("Input pembagi: ");
	int pembagi = int.Parse(Console.ReadLine());

	int mod = angka % pembagi;

	if (mod == 0)
  {
		return $"Angka {angka} % pembagi {pembagi} adalah 0.";
  }
  else
  {
		return $"Angka {angka} % pembagi {pembagi} bukan 0, tetapi {mod}.";
	}
}