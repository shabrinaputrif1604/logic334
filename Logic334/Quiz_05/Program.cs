﻿//Nomor1();
//Nomor2();
//Nomor3();
//Nomor4();
//Nomor5();
//Nomor6();
//Nomor7();
Nomor8();

static void Nomor1()
{
	Console.Write("Golongan:\t");
	int golongan = int.Parse(Console.ReadLine());

	Console.Write("Jam Kerja:\t");
	int jamKerja = int.Parse(Console.ReadLine());

	int upahPerJam = 0;
	int upahLembur = 0;
	int upahMingguan;
	int total;

	if (golongan == 1)
	{
		upahPerJam = 2000;
	}
	else if (golongan == 2)
	{
		upahPerJam = 3000;
	}
	else if (golongan == 3)
	{
		upahPerJam = 4000;
	}
	else if (golongan == 4)
	{
		upahPerJam = 5000;
	}
	else
	{
		Console.WriteLine("Golongan tidak valid");
	}

	if (jamKerja > 40)
	{
		upahLembur = (jamKerja - 40) * upahPerJam * 3 / 2;
		upahMingguan = 40 * upahPerJam;
		total = upahMingguan + upahLembur;
	}
	else
	{
		total = jamKerja * upahPerJam;
		upahMingguan = total;
	}

	Console.WriteLine($"Upah \t {upahMingguan}");
	Console.WriteLine($"Lembur \t {upahLembur}");
	Console.WriteLine($"Total \t {total}");
}

static void Nomor2()
{
	Console.Write("Input: \t");
	string input = Console.ReadLine();

	string split = Console.ReadLine();

	string[] words = input.Split(split);

	for (int i = 0; i < words.Length; i++)
	{
		Console.WriteLine($"Kata {i + 1} = {words[i]}");
	}

	Console.WriteLine($"Total kata adalah {words.Length}");
}

static void Nomor3()
{
	Console.Write("Input: \t");
	string input = Console.ReadLine();

	string[] words = input.Split(" ");

	string word;

	string res = "";

	for(int i = 0; i < words.Length; i++) 
	{
		word = words[i];
		
		for(int j = 0; j < word.Length; j++)
		{
			if (j == 0 || j == word.Length - 1)
			{
				res += word[j];
			}
			else
			{
				res += "*";
			}
		}
		res += " ";
	}
	Console.Write(res);
}

static void Nomor4()
{
	Console.Write("Input: \t");
	string input = Console.ReadLine();

	string[] words = input.Split(" ");

	string word;

	string res = "";

	for (int i = 0; i < words.Length; i++)
	{
		word = words[i];

		for (int j = 0; j < word.Length; j++)
		{
			if (j == 0 || j == word.Length - 1)
			{
				res += "*";
			}
			else
			{
				res += word[j];
			}
		}
		res += " ";
	}
	Console.Write(res);
}

static void Nomor5()
{
	Console.Write("Input: \t");
	string input = Console.ReadLine();

	string[] words = input.Split(" ");

	string word;

	string res = "";

	for (int i = 0; i < words.Length; i++)
	{
		word = words[i];

		for (int j = 0; j < word.Length; j++)
		{
			if (j == 0)
			{
				word.Remove(j);
			}
			else
			{
				res += word[j];
			}
		}
		res += " ";
	}
	Console.Write(res);
}

static void Nomor6()
{
	Console.Write("Input N: ");
	int n = int.Parse(Console.ReadLine());

	int x = 3;

	for (int i = 0; i < n; i++)
	{
    if (i % 2 != 0)
    {
			Console.Write("* ");
		}
		else
		{
			Console.Write($"{x} ");
		}
		x *= 3;
	}
}

static void Nomor7()
{
	Console.Write("Input N: ");
	int n = int.Parse(Console.ReadLine());

	int a = 0, b = 1, c = 1;

	for (int i = 0; i < n; i++)
	{
		Console.Write(c + " ");
		c = a + b;
		a = b;
		b = c;
	}
}

static void Nomor8()
{
	Console.Write("Input N: ");
	int n = int.Parse(Console.ReadLine());

	for(int i = 0; i < n; i++)
	{
		for(int j = 0; j < n; j++)
		{
			if (i == 0)
			{
				Console.Write($"{j + 1}\t");
			} 
			else if(i == n - 1)
			{
				Console.Write($"{n - j}\t");
			}
			else if(j == 0 || j == n - 1)
			{
				Console.Write("*\t");
			}
			else
			{
				Console.Write("\t");
			}
		}
		Console.WriteLine();
	}
}