﻿using Logic04;

//ArrayInit();
//AccessArray();
//TwoDimensionalArray();
//ListInit();
//AccessStudentClass();
//ListAccessElements();
//InsertList();
//IndexElementList();
//InitializeDatetime();
//ParsingDateTime();
//DateTimeProperties();
TimeSpan();

static void ArrayInit()
{
	Console.WriteLine("This is Array Initialization.");

	int[] array = new int[5];
	int[] array2 = new int[5] { 1,2,3,4,5 };
	int[] array3 = new int[] { 1,2,3,4,5 };
	int[] array4 = { 1,2,3,4,5 };
	int[] array5;
	array5 = new int[] { 1,2,3,4,5 };

	Console.WriteLine(string.Join(" , ", array));
	Console.WriteLine(string.Join(" , ", array2));
	Console.WriteLine(string.Join(" , ", array3));
	Console.WriteLine(string.Join(" , ", array4));
	Console.WriteLine(string.Join(" , ", array5));
}

static void AccessArray()
{
	Console.WriteLine("This is Access Array.");

	int[] intArray = new int[3];

	intArray[0] = 1;
	intArray[1] = 2;
	intArray[2] = 3;

	Console.WriteLine(intArray[0]);
	Console.WriteLine(intArray[1]);
	Console.WriteLine(intArray[2]);

	int[] array = { 1, 2, 3, 4 };

	for (int i = 0; i < array.Length; i++)
	{
		Console.WriteLine(array[i]);
	}

	Console.WriteLine();

	string[] strArray = { "Mahesh Chand", "Mike Gold", "Dines Benimal" };

  foreach (var item in strArray)
  {
		Console.WriteLine(item);
  }
}

static void TwoDimensionalArray()
{
	int[,] array = new int[3, 3]
	{
		{ 1, 2, 3 },
		{ 4, 5, 6 },
		{ 7, 8, 9 }
	};

	for (int i = 0; i < array.GetLength(0); i++)
	{
		for(int j = 0; j < array.GetLength(1); j++)
		{
			Console.Write(array[i, j] + "\t");
		}
		Console.WriteLine();
	}
}

static void ListInit()
{
	Console.WriteLine("This is Initialize List.");

	List<string> strList = new List<string>()
	{
		"John Doe",
		"Jane Doe",
		"Joe Doe"
	};

	strList.Add("Joko Doe");

	strList[3] = "joko doe";

	strList.RemoveAt(3);
	strList.Remove("joko doe");

	Console.WriteLine(string.Join(" , ", strList));
}

static void AccessStudentClass()
{
	Console.WriteLine("This is Access Student Class.");

	List<Student> students = new List<Student>()
	{
		new Student(){ Id = 1, Name = "John Doe" },
		new Student(){ Id = 2, Name = "Jane Doe" },
		new Student(){ Id = 3, Name = "Joe Doe" },
	};

	// using Add func
	students.Add(new Student() { Id = 4, Name = "Joko Doe" });

	Student student = new Student();
	student.Id = 5;
	student.Name = "Jimmy Doe";
	students.Add (student);

	Console.WriteLine($"Banyaknya list students adalah {students.Count}");

  foreach (Student item in students)
  {
		Console.WriteLine($"Id: {item.Id}, Name: {item.Name}");
  }

	for(int i = 0; i < students.Count; i++)
	{
		Console.WriteLine($"Id: {students[i].Id}, Name: {students[i].Name}");
	}
}

static void ListAccessElements()
{
	Console.WriteLine("This is List Access Elements.");

	List<int> intList = new List<int>();
	intList.Add(1);
	intList.Add(2);
	intList.Add(3);

	Console.WriteLine(intList[0]);
	Console.WriteLine(intList[1]);
	Console.WriteLine(intList[2]);

  foreach (int item in intList)
  {
		Console.WriteLine(item);
  }

	Console.WriteLine();

	for(int i = 0; i <  intList.Count; i++)
	{
		Console.WriteLine(intList[i]);
	}
}

static void InsertList()
{
	Console.WriteLine("This is Insert List.");

	List<int> intList = new List<int>();
	intList.Add(1);
	intList.Add(2);
	intList.Add(3);

	//Insert(index, value)
	intList.Insert(1, 4);

	for(int i = 0; i < intList.Count; i++)
	{
		Console.WriteLine(intList[i]);
	}
}

static void IndexElementList()
{
	Console.WriteLine("This is IndexElementList.");

	List<string> list = new List<string>();
	list.Add("1");
	list.Add("2");
	list.Add("3");

	Console.Write("Insert an element: ");
	string item = Console.ReadLine();

	int index = list.IndexOf(item);

  if (index != -1)
  {
		Console.WriteLine($"Element {item} is found at index {index}");
	}
	else
	{
		Console.WriteLine($"Element {item} is not found");
	}
}

static void InitializeDatetime()
{
	Console.WriteLine("This is InitializeDatetime.");

	DateTime dt1 = new DateTime();
	Console.WriteLine(dt1);

	DateTime dt2 = DateTime.Now.Date;
	Console.WriteLine(dt2);
	Console.WriteLine(dt2.ToString("dd/MMM/yyyy"));

	DateTime dt3 = new DateTime(2023, 11, 1);
	Console.WriteLine(dt3);

	DateTime dt4 = new DateTime(2023, 11, 1, 10, 43, 25);
	Console.WriteLine(dt4);
}

static void ParsingDateTime()
{
	Console.WriteLine("This is ParsingDateTime.");

	Console.Write("Input a date (d/MM/yyyy):");
	string strDt = Console.ReadLine();
	
	try
	{
		DateTime data1 = DateTime.ParseExact(strDt, "dd/MM/yyyy", null);
		Console.WriteLine(data1);
	}catch (Exception e)
	{
		Console.WriteLine("Wrong format");
		Console.WriteLine($"Error msg: {e.Message}");
	}
}

static void DateTimeProperties()
{
	Console.WriteLine("This is DateTimeProperties.");

	DateTime date = new DateTime(2023, 11, 1, 11, 10, 25);

	int year = date.Year;
	int month = date.Month;
	int day = date.Day;
	int hour = date.Hour;
	int minute = date.Minute;
	int second = date.Second;
	int weekday = (int)date.DayOfWeek;

	string strDay = date.DayOfWeek.ToString();
	string strDay2 = date.ToString("dddd");

	Console.WriteLine($"Tahun: {year}");
	Console.WriteLine($"Bulan: {month}");
	Console.WriteLine($"Hari: {day}");
	Console.WriteLine($"Jam: {hour}");
	Console.WriteLine($"Menit: {minute}");
	Console.WriteLine($"Detik: {second}");
	Console.WriteLine($"Weekday: {weekday}");
	Console.WriteLine($"Hari string: {strDay}");
	Console.WriteLine($"Hari string 2: {strDay2}");
}

static void TimeSpan()
{
	Console.WriteLine("This is TimeSpan.");

	DateTime date1 = new DateTime(2016, 1, 10, 11, 20, 30);
	DateTime date2 = new DateTime(2016, 2, 20, 12, 25, 35);

	TimeSpan interval = date2 - date1;

	Console.WriteLine("Number of days: " + interval.Days);
	Console.WriteLine("Total number of days: " + interval.TotalDays);

	Console.WriteLine("Number of hours: " + interval.Hours);
	Console.WriteLine("Total number of hours: " + interval.TotalHours);

	Console.WriteLine("Number of minutes: " + interval.Minutes);
	Console.WriteLine("Total number of minutes: " + interval.TotalMinutes);

	Console.WriteLine("Number of seconds: " + interval.Seconds);
	Console.WriteLine("Total number of seconds: " + interval.TotalSeconds);

	Console.WriteLine("Number of milliseconds: " + interval.Milliseconds);
	Console.WriteLine("Total number of milliseconds: " + interval.TotalMilliseconds);

	Console.WriteLine("Total number of ticks: " + interval.Ticks);
}