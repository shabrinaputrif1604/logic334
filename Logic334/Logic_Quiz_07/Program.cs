﻿//Nomor1();
//Nomor2();
//Nomor3();
//Nomor4();
//Nomor5();
//Nomor6();
//Nomor7();
//Nomor8();
Nomor9();
//Nomor10();

static void Nomor1()
{
	Console.Write("Masukkan banyak orang: ");
	int x = int.Parse(Console.ReadLine());
	int y = x;
	//int z = x;

	int total = 1;

	//string res = "";

	//for (int i = 0; i < y; i++)
	//{
	//	total *= y;
	//	y -= 1;
	//}

	//for(int i = 0; i < x; i++)
	//{
	//	res += x + " x ";
	//	x -= 1;
	//	if(x == 1)
	//	{
	//		res += x;
	//	}

	//}

	string detail = "";
	for(int i = x; i > 0; i--)
	{
		total *= i;
		detail += detail == "" ? i : " x " + i;
	}

	Console.WriteLine($"{y}! = {detail} = {total}");
	Console.Write($"Ada {total} cara untuk duduk");
	//Console.WriteLine($"{z}! = {res} = {total}");

}

static void Nomor2()
{
	Console.Write("Masukkan kode: ");
	string code = Console.ReadLine().ToUpper();

	string res = "";

	int wrongCode = 0, trueCode = 0;

	for (int i = 0; i < code.Length; i++)
	{
		if (i % 3 == 0)
		{
			if (code.Substring(i, 3) != "SOS")
			{
				wrongCode += 1;
				res += "SOS";
			}
			else
			{
				trueCode += 1;
				res += "SOS";
			}
		}
	}

	Console.WriteLine($"Sinyal seharusnya: {res}");

	//for (int i = 0; i < code.Length; i++)
	//{
	//	try
	//	{
	//		if (i % 3 == 0)
	//		{
	//			if (code.Substring(i, 3) != "SOS")
	//			{
	//				wrongCode += 1;
	//			}
	//		}
	//	}
	//	catch (Exception e)
	//	{
	//		Console.WriteLine(e);
	//		wrongCode += 1;
	//	}
	//}

	Console.WriteLine($"Total sinyal salah: {wrongCode}");
	Console.WriteLine($"Total sinyal benar: {trueCode}");
}

static void Nomor3()
{
	Console.Write("Masukkan tanggal pinjam (dd/MM/yyyy): ");
	DateTime dtPinjam = DateTime.Parse(Console.ReadLine());

	Console.Write("Masukkan tanggal kembali (dd/MM/yyyy): ");
	DateTime dtKembali = DateTime.Parse(Console.ReadLine());

	TimeSpan interval = dtKembali - dtPinjam;

	int denda = 0;

	int banyakHari = Convert.ToInt32(interval.ToString("dd"));
	//Console.WriteLine(banyakHari);

	if(banyakHari <= 3)
	{
		denda = 0;
	}
	else
	{
		banyakHari -= 3;
		denda = banyakHari * 500;
	}

	Console.WriteLine($"Keterlambatan: {banyakHari} hari");
	Console.WriteLine($"Denda kamu adalah: {denda.ToString("C")}");
	Console.WriteLine($"Denda kamu adalah: Rp. {denda.ToString("#,#.00")}");
}

static void Nomor4()
{
	Console.Write("Masukkan tanggal mulai: ");
	DateTime dtStart = DateTime.Parse(Console.ReadLine());

	Console.Write("Masukkan tanggal libur: ");
	string dtHoliday = Console.ReadLine();
	string[] arrayHoliday = dtHoliday.Split(",");

	int i = 0, totalDays = 0;
	while (i < 10)
	{
		if (!String.Equals(dtStart.AddDays(totalDays).ToString("dddd"), "Saturday") &&
			!String.Equals(dtStart.AddDays(totalDays).ToString("dddd"), "Sunday") &&
			!arrayHoliday.Contains(dtStart.AddDays(totalDays).ToString("dd")))
		{
			i++;
			//Console.WriteLine(dtStart.AddDays(totalDays).ToString("ddddMM"));
		}
		totalDays++;
	}
	Console.WriteLine("Kelas akan tes pada " + dtStart.AddDays(totalDays).ToString("dd/MM/yyyy"));
}

static void Nomor5()
{
	Console.Write("Masukkan sebuah kalimat: ");
	string sentence = Console.ReadLine().ToLower();

	//char[] arraySen = sentence.ToCharArray();

	int totalVokal = 0, totalKonsonan = 0;

	for(int i = 0; i < sentence.Length; i++)
	{
    if (
			sentence[i] == 'a' ||
			sentence[i] == 'i' ||
			sentence[i] == 'u' ||
			sentence[i] == 'e' ||
			sentence[i] == 'o'
			)
    {
			totalVokal += 1;
    }
		else if (sentence[i] == ' ')
		{
			continue;
		}
		else
		{
			totalKonsonan += 1;
		}
   }

	Console.WriteLine($"Total huruf vokal: {totalVokal}");
	Console.WriteLine($"Total huruf konsonan: {totalKonsonan}");
}

static void Nomor6()
{
	Console.Write("Masukkan nama Anda: ");
	string name = Console.ReadLine();

	for(int i =  0; i < name.Length; i++)
	{
		if (char.IsLetter(name[i]))
		{
			Console.WriteLine($"***{name[i]}***");
		}
	}
}

static void Nomor7()
{
	Console.Write("Total menu: ");
	int totalMenu = int.Parse(Console.ReadLine());

	Console.Write("Indeks menu alergi: ");
	int index = int.Parse(Console.ReadLine());

	Console.Write("Harga menu (pemisah (,)): ");
	string inputHarga = Console.ReadLine();
	string[] splitHarga = inputHarga.Split(",");
	int[] harga = Array.ConvertAll(splitHarga, int.Parse);

	Console.Write("Uang Elsa: ");
	int uang = int.Parse(Console.ReadLine());

	int totalAll = 0;

	for(int i = 0; i < harga.Length; i++)
	{
		if(i != index)
		{
			totalAll += harga[i];
		}
	}

	int bayar = totalAll / 2;
	int sisaUang = uang - bayar;

	Console.WriteLine($"Elsa harus membayar: {bayar}");

	if (sisaUang == 0)
	{
		Console.WriteLine("Uang pas.");
	}
	else if(sisaUang > 0)
	{
		Console.WriteLine($"Sisa uang: {sisaUang}");
	}
	else
	{
		Console.WriteLine($"Uang kurang {sisaUang}");
	}
}

static void Nomor8()
{
	Console.Write("Masukkan angka: ");
	int n = int.Parse(Console.ReadLine());

	//for(int i = n; i > 0; i--)
	//{
	//	for (int j = 0; j <= n; j++)
	//	{
	//		if(j >= i)
	//		{
	//			Console.Write("#");
	//		}
	//		else
	//		{
	//			Console.Write(" ");
	//		}
	//	}

	//	Console.WriteLine();
	//}

	for(int i = 1; i <= n; i++)
	{
		Console.WriteLine(new string(' ', n - i) + new string('#', i));
	}
}

static void Nomor9()
{
	Console.Write("Masukkan panjang baris dan kolom (mis. 3): ");
	int n = int.Parse(Console.ReadLine());

	int[,] matrix = new int[n, n];

	Console.WriteLine("Masukkan isi array: ");

	for (int i = 0; i < n; i++)
	{
		for (int j = 0; j < n; j++)
		{
			Console.Write($"Masukkan isi pada baris {i}, kolom {j}: ");
			matrix[i, j] = int.Parse(Console.ReadLine());
		}
	}

	for (int i = 0; i < matrix.GetLength(0); i++)
	{
		for (int j = 0; j < matrix.GetLength(1); j++)
		{
			Console.Write(matrix[i, j] + " ");
		}
		Console.WriteLine();
	}

	//int diagonal1 = 0, diagonal2 = 0;

	//for (int i = 0; i < matrix.GetLength(0); i++)
	//{
	//	for(int j = 0; j < matrix.GetLength(1); j++)
	//	{
	//		if(i == j)
	//		{
	//			diagonal1 += matrix[i, j];
	//		}

	//		if (i == matrix.GetLength(1) - 1 - j)
	//		{
	//			diagonal2 += matrix[i, j];
	//		}
	//	}
	//}

	//int diff = Math.Abs(diagonal1 - diagonal2);
	//Console.WriteLine(diff);
	
}

static void Nomor10()
{ 
	Console.Write("Masukkan input (pemisah spasi): ");
	string input = Console.ReadLine();
	string[] splitInput = input.Split(" ");
	int[] array = Array.ConvertAll(splitInput, int.Parse);

	int temp = 0, total = 0;

	for(int i = 0; i < array.Length; i++)
	{
		if(temp < array[i])
		{
			temp = array[i];
		}
	}

	for(int i = 0; i < array.Length; i++)
	{
		if (array[i] == temp)
		{
			total += 1;
		}
	}

	Console.WriteLine("Ini total " + total);
}