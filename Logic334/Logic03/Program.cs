﻿//WhileLoop();
//WhileLoop2();
//DoWhileLoop();
//ForLoop();
//Break();
//Continue();
//ForNestedLoop();
//ForEachLoop();
//Length();
RemoveStr();
//InsertStr();
//ReplaceStr();
//SplitJoin();
//Substring();
//ContainsStr();
//ToCharArray();
//ConvertAll();

static void WhileLoop()
{
	Console.WriteLine("This is While Loop.");

	Console.Write("Insert a value: ");
	int val = int.Parse(Console.ReadLine());

    while (val < 6)
    {
    Console.WriteLine(val);
    val++;
    }
}

static void WhileLoop2()
{
	Console.WriteLine("This is Second While Loop.");

	Console.Write("Insert a value: ");
	int val = int.Parse(Console.ReadLine());

	bool loop = true;

	while (loop)
	{
		Console.WriteLine($"Process {val}");
		val++;

		Console.WriteLine("Do you want to continue the loop? (y/n)");
		string ans = Console.ReadLine();

		if(ans.ToLower() == "n")
		{
			loop = false;
		}
	}
}

static void DoWhileLoop()
{
	Console.WriteLine("This is Do While Loop.");

	Console.Write("Insert a value: ");
	int val = int.Parse(Console.ReadLine());

	do{
		Console.WriteLine(val);
		val++;
	} 
	while (val < 6) ;

}

static void ForLoop()
{
	Console.WriteLine("This is For While Loop.");

	Console.Write("Insert a value: ");
	int val = int.Parse(Console.ReadLine());

	for(int i = 0; i < val; i++)
	{
		Console.Write(i + "\t");
	}

	Console.Write("\n");

	for (int i = val; i > 0; i--)
	{
		Console.Write(i + "\t");
	}
}

static void Break(){
	for(int i = 0; i < 10; i++)
	{
		Console.WriteLine(i);
		if(i == 6)
		{
			break;
		}
	}
}

static void Continue()
{
	for (int i = 0; i < 10; i++)
	{
		if (i >= 2 && i <= 7)
		{
			continue; ;
		}
		Console.WriteLine(i);
	}
}

static void ForNestedLoop()
{
	for(int i = 0; i < 3; i++)
	{
		for(int j = 0; j < 3; j++)
		{
			Console.Write($"({i}, {j}) \t");
		}
		Console.WriteLine();
	}
}

static void ForEachLoop()
{
	int[] array = { 89, 90, 91, 92, 93 };

	int res = 0;

  foreach (var item in array)
  {
		res += item;
  }
	Console.WriteLine(res);
}

static void Length()
{
	Console.WriteLine("This is Length.");

	Console.Write("Insert a word: ");
	string val = Console.ReadLine().ToUpper();

	Console.WriteLine($"The length of {val} word is {val.Length}");
}

static void RemoveStr()
{
	Console.WriteLine("This is Remove String.");

	Console.Write("Insert a word: ");
	string val = Console.ReadLine().ToUpper();

	Console.Write("Insert index: ");
	int rmv = int.Parse(Console.ReadLine()); 

	Console.WriteLine($"{val.Remove(rmv, 1)}");
}

static void InsertStr()
{
	Console.WriteLine("This is Insert String.");

	Console.Write("Insert a word: ");
	string val = Console.ReadLine().ToUpper();

	Console.Write("Insert index: ");
	int index = int.Parse(Console.ReadLine());

	Console.WriteLine($"{val.Insert(index, "uhuy")}");
}

static void ReplaceStr()
{
	Console.WriteLine("This is Replace String.");

	Console.Write("Insert a word: ");
	string val = Console.ReadLine();

	Console.Write("Insert the old word: ");
	string valOld = Console.ReadLine();

	Console.Write("Insert a new word: ");
	string valNew = Console.ReadLine();

	Console.WriteLine($"{val.Replace(valOld, valNew)}");
}

static void SplitJoin()
{
	Console.WriteLine("This is Split and Join");

	Console.Write("Insert a sentence: ");
	string sentence = Console.ReadLine();

	Console.Write("Insert split symbol: ");
	string split = Console.ReadLine();

	string[] arrayWord = sentence.Split(split);

  foreach (var item in arrayWord)
  {
		Console.WriteLine(item);
  }

	Console.WriteLine(string.Join(" ", arrayWord));

	Console.WriteLine();

	int[] deret = { 1, 2, 3, 4, 5, 6 };
	Console.WriteLine(String.Join(" + ", deret));
}

static void Substring()
{
	Console.WriteLine("This is Substring.");

	Console.Write("Insert a Code: ");
	string code = Console.ReadLine();

	Console.Write("Insert Parameter 1: ");
	int param1 = int.Parse(Console.ReadLine());

	Console.Write("Insert Parameter 2: ");
	int param2 = int.Parse(Console.ReadLine());

	if(param2 == 0)
	{
		Console.WriteLine($"Hasil substring = {code.Substring(param1)}");
	}
	else
	{
		Console.WriteLine($"Hasil substring = {code.Substring(param1, param2)}");
	}
}

static void ContainsStr()
{
	Console.WriteLine("This is Contain String.");

	Console.Write("Insert a word: ");
	string word = Console.ReadLine();

	Console.Write("Insert a contains word: ");
	string contain = Console.ReadLine();

	if (word.Contains(contain))
	{
		Console.WriteLine($"Kata {word} mengandung {contain}");
	}
	else
	{
		Console.WriteLine($"Kata {word} tidak mengandung {contain}");
	}
}

static void ToCharArray()
{
	Console.WriteLine("This is To Char Array.");

	Console.Write("Insert a sentence: ");
	string sentence = Console.ReadLine();

	char[] array = sentence.ToCharArray();

  foreach (var item in array)
  {
		Console.WriteLine(item);   
  }

	Console.WriteLine();

	for(int i = 0; i < array.Length; i++)
	{
		Console.WriteLine(array[i]);
	}
}

static void ConvertAll()
{
	Console.WriteLine("This is Convert All Array.");

	Console.Write("Insert some integer (using comma): ");
	string[] inputNum = Console.ReadLine().Split(",");

	int res = 0;

	int[] array = Array.ConvertAll(inputNum, int.Parse);

  foreach (var item in array)
  {
		res += item;
  }

	Console.Write($"The result is {res}");
}