﻿// See https://aka.ms/new-console-template for more information
Console.Write("Masukkan Nama: ");

// Scan input
string nama = Console.ReadLine();
string nama2 = Console.ReadLine();

// Call the variable
Console.WriteLine("Hello, " + nama);
Console.WriteLine("Met dateng, {0} dan {1}", nama, nama2);
Console.WriteLine($"Hai, {nama} dan {nama2}!!");

// Test print variable
int age = 21;
Console.WriteLine(age.ToString());

bool isFine = false;
Console.WriteLine(isFine.ToString());

// Mutable and Immutable variable
// var is an implisit datatype, just use it when we dont know the datatype that we want to use
var test = "uhuy";
Console.WriteLine(test);

const string test2 = "ini adalah testing const";
Console.WriteLine(test2);

Console.ReadKey();