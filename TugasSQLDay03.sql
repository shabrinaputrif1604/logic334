CREATE DATABASE DB_Sales;

USE DB_Sales;

CREATE TABLE salesperson(
	id INT PRIMARY KEY IDENTITY (1,1),
	name VARCHAR (50) NOT NULL,
	bod DATE NOT NULL,
	salary DECIMAL(18,2)
);

INSERT INTO salesperson(name, bod, salary) VALUES
	('Abe', '1988-11-9', 140000),
	('Bob', '1978-11-9', 44000),
	('Chris', '1983-11-9', 40000),
	('Dan', '1980-11-9', 52000),
	('Ken', '1977-11-9', 115000),
	('Joe', '1990-11-9', 38000)

UPDATE salesperson SET bod = '9-11-1990' WHERE id = 6;

SELECT * FROM salesperson;

CREATE TABLE orders (
	id INT PRIMARY KEY IDENTITY (1,1),
	order_date DATE NOT NULL,
	cust_id INT NOT NULL,
	salesperson_id INT NOT NULL,
	amount DECIMAL (18,2)
)

INSERT INTO orders (order_date, cust_id, salesperson_id, amount) VALUES
	('8/2/2020', 4, 2, 540),
	('1/22/2021', 4, 5, 1800),
	('7/14/2019', 9, 1, 460),
	('1/29/2018', 7, 2, 2400),
	('2/3/2021', 6, 4, 600),
	('3/2/2020', 6, 4, 720),
	('5/6/2021', 9, 4, 150)
 
--a. Informasi nama sales yang memiliki order lebih dari 1.					
SELECT sales.name, COUNT(sales.name) as count_order FROM salesperson as sales
	JOIN orders on sales.id = orders.salesperson_id
	GROUP BY sales.name
	HAVING COUNT(sales.name) > 1;

--b. Informasi nama sales yang total amount ordernya di atas 1000.
SELECT sales.name, SUM(orders.amount) as sum_amount FROM salesperson as sales
	JOIN orders on sales.id = orders.salesperson_id
	GROUP BY sales.name
	HAVING SUM(orders.amount) > 1000;

--c. Informasi nama sales, umur, gaji dan total amount order yang tahun ordernya >= 2020 dan data ditampilan berurut sesuai dengan umur (ascending).
SELECT sales.name, DATEDIFF(YEAR, sales.bod, GETDATE()) as age, sales.salary, SUM(orders.amount) as total_amount
	FROM salesperson as sales
	LEFT JOIN orders on sales.id = orders.salesperson_id
	WHERE YEAR(orders.order_date) >= 2020
	GROUP BY sales.name, sales.bod, sales.salary;

--d. Carilah rata-rata total amount masing-masig sales urutkan dari hasil yg paling besar								
SELECT sales.name, AVG(orders.amount) as avg_amount FROM orders
	JOIN salesperson as sales on orders.salesperson_id = sales.id
	GROUP BY sales.name
	ORDER BY avg_amount DESC;

--e. perusahaan akan memberikan bonus bagi sales yang berhasil memiliki order lebih dari 2 dan total order lebih dari 1000 sebanyak 30% dari salary
SELECT sales.name, COUNT(salesperson_id) as count_order, 
	SUM(orders.amount) as total_amount, 
	(sales.salary + (sales.salary * 30/100)) as total_salary FROM orders 
	JOIN salesperson as sales on orders.salesperson_id = sales.id
	GROUP BY sales.name, sales.salary
	HAVING COUNT(salesperson_id) > 2 AND SUM(orders.amount) > 1000;

--f. Tampilkan data sales yang belum memiliki orderan sama sekali						
SELECT * FROM salesperson as sales
	LEFT JOIN orders on sales.id = orders.salesperson_id
	WHERE orders.salesperson_id IS NULL;

--g. Gaji sales akan dipotong jika tidak memiliki orderan,  gaji akan di potong sebanyak 2%								
SELECT * FROM orders;
SELECT * FROM salesperson;
SELECT sales.name, (sales.salary - (sales.salary * 2/100)) as total_salary FROM salesperson as sales
	LEFT JOIN orders on sales.id = orders.salesperson_id
	WHERE orders.salesperson_id IS NULL;