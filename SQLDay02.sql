--SQLDay02

USE db_kampus

--in
SELECT * FROM mahasiswa WHERE name = 'Tunggul' OR name = 'Irvan';
SELECT * FROM mahasiswa WHERE name IN ('tunggul', 'irvan');

ALTER TABLE mahasiswa ADD nilai int;

SELECT * FROM mahasiswa;

--update column nilai
UPDATE mahasiswa SET nilai = 80 WHERE id = 1;
UPDATE mahasiswa SET nilai = 80 WHERE id = 2;
UPDATE mahasiswa SET nilai = 70 WHERE id = 3;
UPDATE mahasiswa SET nilai = 100 WHERE id = 4;
UPDATE mahasiswa SET nilai = 70 WHERE id = 5;
UPDATE mahasiswa SET nilai = 50 WHERE id = 6;
UPDATE mahasiswa SET nilai = 40 WHERE id = 8;

--avg sum
SELECT avg(nilai) FROM mahasiswa;
SELECT sum(nilai), name FROM mahasiswa GROUP BY name;

--inner join/join
SELECT * FROM mahasiswa as mhs
	INNER JOIN biodata as bio
	on mhs.id = bio.mahasiswa_id

--left join 
SELECT * FROM mahasiswa as mhs
	INNER JOIN biodata as bio
	on mhs.id = bio.mahasiswa_id
	WHERE bio.id IS NULL

--right join 
SELECT * FROM mahasiswa as mhs
	INNER JOIN biodata as bio
	on mhs.id = bio.mahasiswa_id
	WHERE mhs.id IS NULL

--distinct
SELECT DISTINCT name FROM mahasiswa

--group by
SELECT name FROM mahasiswa GROUP BY name

--substring
SELECT SUBSTRING('SQL Tutorial', 1, 3);

--charindex
SELECT CHARINDEX('T', 'customer');

--datalength
SELECT DATALENGTH('W3schools.com');

--case when
SELECT name, nilai, 
	CASE 
	WHEN nilai >= 80 then 'A'
	WHEN nilai >= 60 then 'B'
	ELSE 'C'
	END
as grade FROM mahasiswa

SELECT name, nilai, 
	CASE 
	WHEN nilai >= 80 then CASE WHEN name = 'imam' then 'A' else 'x' end
	WHEN nilai >= 60 then 'B'
	ELSE 'C'
	END
as grade FROM mahasiswa

--concat
SELECT CONCAT('SQL', 'is', 'fun')
SELECT 'SQL' + 'is' + 'fun'

CREATE TABLE penjualan (
	id INT PRIMARY KEY IDENTITY (1,1),
	name VARCHAR (50) NOT NULL,
	harga DECIMAL (18,0) NOT NULL
)

INSERT INTO penjualan (name, harga) VALUES
	('Indomie', 1500),
	('Closeup', 3500),
	('Pepsodent', 3000),
	('Brush Formula', 2500),
	('Roti Manis', 1000),
	('Gula', 3500),
	('Sarden', 4500),
	('Rokok Sampurna', 11000),
	('Rokok 234', 11000)


--getdate
SELECT GETDATE();

--day, month, year
SELECT DAY('2023-11-10');
SELECT MONTH('2023-11-10');
SELECT YEAR(GETDATE());

--dateadd
SELECT DATEADD(YEAR, 1, '2023-11-10')
SELECT DATEADD(MONTH, 1, '2023-11-10')
SELECT DATEADD(DAY, 5, '2023-11-10')
SELECT DATEADD(HOUR, 5, GETDATE());

--datediff
SELECT DATEDIFF(YEAR, '2023-11-10', '2024-11-10');
SELECT DATEDIFF(MONTH, '2023-11-10', '2024-11-10');
SELECT DATEDIFF(DAY, '2023-11-10', DATEADD(DAY, 5, GETDATE()) );

--SUBQUERY
SELECT * 
FROM mahasiswa mhs
LEFT JOIN biodata bio on mhs.id = bio.mahasiswa_id
WHERE mhs.nilai = (SELECT MAX(nilai) FROM mahasiswa)

CREATE TABLE mahasiswa_new(
	id bigint PRIMARY KEY IDENTITY (1,1),
	name VARCHAR (50) NOT NULL,
	address VARCHAR (50) NOT NULL, 
	email VARCHAR (255) NULL
);

INSERT INTO mahasiswa_new (name, address, email)
SELECT name, address, email FROM mahasiswa

--create index
CREATE INDEX index_nameemail on mahasiswa(name, email)

--create unique index
CREATE UNIQUE INDEX index_id on mahasiswa(name, email)

--drop index
DROP INDEX index_id on mahasiswa;

--primary key
CREATE TABLE coba(id int not null, nama varchar(50) not null)
ALTER TABLE coba ADD CONSTRAINT pk_idnama PRIMARY KEY (id, nama)

INSERT INTO coba(id,nama) VALUES 
(3, 'Irvan')

ALTER TABLE coba DROP CONSTRAINT pk_idnama

DELETE coba

--unique key
ALTER TABLE coba ADD CONSTRAINT unique_nama unique(nama)

--foreign key
ALTER TABLE biodata ADD CONSTRAINT fk_mahasiswa_id FOREIGN KEY(mahasiswa_id) REFERENCES mahasiswa(id)

--drop unique key 
ALTER TABLE biodata DROP CONSTRAINT fk_mahasiswa_id