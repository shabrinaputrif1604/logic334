--TugasSQLDay02
CREATE DATABASE db_entertainer;

USE db_entertainer;

--ARTIS
CREATE TABLE artis
(
	id INT PRIMARY KEY IDENTITY (1,1),
	kd_artis VARCHAR (100) NOT NULL,
	nm_artis VARCHAR (100) NOT NULL,
	jk VARCHAR (100) NOT NULL,
	bayaran INT NOT NULL,
	award INT NOT NULL, 
	negara VARCHAR (100) NOT NULL
);

ALTER TABLE artis ALTER COLUMN bayaran BIGINT NOT NULL;

INSERT INTO artis (kd_artis, nm_artis, jk, bayaran, award, negara)
	VALUES
	('A001', 'ROBERT DOWNEY JR', 'PRIA', 3000000000, 2, 'AS'),
	('A002', 'ANGELINA JOLIE', 'WANITA', 700000000, 1, 'AS'),
	('A003', 'JACKIE CHAN', 'PRIA', 200000000, 7, 'HK'),
	('A004', 'JOE TASLIM', 'PRIA', 350000000, 1, 'ID'),
	('A005', 'CHELSEA ISLAN', 'WANITA', 300000000, 0, 'ID');

SELECT * FROM artis;

--FILM
CREATE TABLE film
(
	id INT PRIMARY KEY IDENTITY (1,1),
	kd_film VARCHAR (10) NOT NULL,
	nm_film VARCHAR (55) NOT NULL,
	genre VARCHAR (55) NOT NULL,
	artis VARCHAR (55) NOT NULL,
	produser VARCHAR (55) NOT NULL, 
	pendapatan BIGINT NOT NULL,
	nominasi INT NOT NULL
);

SELECT * FROM film;

INSERT INTO film (kd_film, nm_film, genre, artis, produser, pendapatan, nominasi)
	VALUES
	('F001', 'IRON MAN', 'G001', 'A001', 'PD01', 2000000000, 3),
	('F002', 'IRON MAN 2', 'G001', 'A001', 'PD01', 1800000000, 2),
	('F003', 'IRON MAN 3', 'G001', 'A001', 'PD01', 1200000000, 0),
	('F004', 'AVENGER:CIVIL WAR', 'G001', 'A001', 'PD01', 2000000000, 1),
	('F005', 'SPIDERMAN HOME COMING', 'G001', 'A001', 'PD01', 1300000000, 0),
	('F006', 'THE RAID', 'G001', 'A004', 'PD03', 800000000, 5),
	('F007', 'FAST AND FURIOUS', 'G001', 'A004', 'PD05', 830000000, 2),
	('F008', 'HABIBIE DAN AINUN', 'G004', 'A005', 'PD03', 670000000, 4),
	('F009', 'POLICE STORY', 'G001', 'A003', 'PD02', 700000000, 3),
	('F010', 'POLICE STORY 2', 'G001', 'A003', 'PD02', 710000000, 1),
	('F011', 'POLICE STORY 3', 'G001', 'A003', 'PD02', 615000000, 0),
	('F012', 'RUSH HOUR', 'G003', 'A003', 'PD05', 695000000,2),
	('F013', 'KUNGFU PANDA', 'G003', 'A003', 'PD05', 923000000, 5);

UPDATE film SET produser = substring(produser,1,2) + '0' + substring(produser,3,2)
select substring('PD01',1,2) + '0' + substring('PD01',3,2)

SELECT produser, nm_film FROM film WHERE kd_film = 'F001' OR kd_film = 'F002' OR kd_film = 'F003' OR kd_film = 'F004' OR kd_film = 'F005'
UPDATE film SET produser = 'PD001' WHERE kd_film = 'F001' OR kd_film = 'F002' OR kd_film = 'F003' OR kd_film = 'F004' OR kd_film = 'F005'

SELECT produser, nm_film FROM film WHERE kd_film = 'F006' OR kd_film = 'F008'
UPDATE film SET produser = 'PD003' WHERE kd_film = 'F006' OR kd_film = 'F008'

SELECT produser, nm_film FROM film WHERE kd_film = 'F007' OR kd_film = 'F012' OR kd_film = 'F013'
UPDATE film SET produser = 'PD005' WHERE kd_film = 'F007' OR kd_film = 'F012' OR kd_film = 'F013'

SELECT produser, nm_film FROM film WHERE kd_film = 'F009' OR kd_film = 'F010' OR kd_film = 'F011'
UPDATE film SET produser = 'PD002' WHERE kd_film = 'F009' OR kd_film = 'F010' OR kd_film = 'F011'

SELECT * FROM film;

--PRODUSER
CREATE TABLE produser (
	id INT PRIMARY KEY IDENTITY (1,1),
	kd_produser VARCHAR (50) NOT NULL,
	nm_produser VARCHAR (50) NOT NULL,
	international VARCHAR (50) NOT NULL
);

INSERT INTO produser (kd_produser, nm_produser, international) VALUES
	('PD001', 'MARVEL', 'YA'),
	('PD002', 'HONGKONG CINEMA', 'YA'),
	('PD003', 'RAPI FILM', 'TIDAK'),
	('PD004', 'PARKIT', 'TIDAK'),
	('PD005', 'PARAMOUNT PICTURE', 'YA');

SELECT * FROM produser;

CREATE TABLE negara (
	id INT PRIMARY KEY IDENTITY (1,1),
	kd_negara VARCHAR (100),
	nm_negara VARCHAR (100)
);

INSERT INTO negara (kd_negara, nm_negara) VALUES 
	('AS', 'AMERIKA SERIKAT'),
	('HK', 'HONGKONG'),
	('ID', 'INDONESIA'),
	('IN', 'INDIA');

SELECT * FROM negara;

CREATE TABLE genre (
	id INT PRIMARY KEY IDENTITY (1,1),
	kd_genre VARCHAR (50),
	nm_genre VARCHAR (50)
);

INSERT INTO genre (kd_genre, nm_genre) VALUES
	('G001', 'ACTION'),
	('G002', 'HORROR'),
	('G003', 'COMEDY'),
	('G004', 'DRAMA'),
	('G005', 'THRILLER'),
	('G006', 'FICTION');

SELECT * FROM genre;
SELECT * FROM film;

--1. Menampilkan jumlah pendapatan produser marvel secara keseluruhan											
SELECT produser.nm_produser, SUM(film.pendapatan) as pendapatan 
	FROM film 
	JOIN produser
	on film.produser = produser.kd_produser
	WHERE produser.nm_produser = 'MARVEL'
	GROUP BY nm_produser;
	--HAVING produser.nm_produser = 'MARVEL'

--2. Menampilkan nama film dan nominasi yang tidak mendapatkan nominasi
SELECT nm_film, nominasi FROM film WHERE nominasi = 0;
SELECT nm_film, nominasi FROM film WHERE nominasi = 0 AND nm_film LIKE '%3';

--3. Menampilkan nama film yang huruf depannya 'p'									
SELECT nm_film FROM film WHERE nm_film LIKE 'p%';
SELECT nm_film FROM film WHERE SUBSTRING(nm_film, 1, 1) = 'p';

--4. Menampilkan nama film yang huruf terakhir 'y'								
SELECT nm_film FROM film WHERE nm_film LIKE '%y';
SELECT nm_film FROM film WHERE SUBSTRING(nm_film, DATALENGTH(nm_film), 1) = 'y';

--5. Menampilkan nama film yang mengandung huruf 'd'									
SELECT nm_film FROM film WHERE nm_film LIKE '%d%';

--6. Menampilkan nama film dan artis						
SELECT film.nm_film, artis.nm_artis FROM film 
	LEFT JOIN artis 
	on film.artis = artis.kd_artis
SELECT film.nm_film, artis.nm_artis FROM film 
	LEFT JOIN artis 
	on film.artis = artis.kd_artis
	WHERE film.nm_film LIKE '%IRON%' OR artis.nm_artis LIKE '%JACKIE%';

--7. Menampilkan nama film yang artisnya berasal dari negara hongkong
SELECT film.nm_film, negara.kd_negara FROM film 
	JOIN artis on film.artis = artis.kd_artis
	JOIN negara on artis.negara = negara.kd_negara
	WHERE artis.negara = 'HK'
SELECT film.nm_film, negara.kd_negara, negara.nm_negara FROM film 
	JOIN artis on film.artis = artis.kd_artis
	JOIN negara on artis.negara = negara.kd_negara
	WHERE artis.negara = 'HK';
SELECT film.nm_film, negara.kd_negara, negara.nm_negara FROM film 
	JOIN artis on film.artis = artis.kd_artis
	JOIN negara on artis.negara = negara.kd_negara
	WHERE artis.negara IN ('HK', 'ID', 'IN');

--8. Menampilkan nama film yang artisnya bukan berasal dari negara yang mengandung huruf 'o'											
SELECT film.nm_film, negara.nm_negara FROM film 
	JOIN artis on film.artis = artis.kd_artis
	JOIN negara on artis.negara = negara.kd_negara
	WHERE negara.nm_negara NOT LIKE '%o%';
SELECT film.nm_film, negara.nm_negara FROM film 
	JOIN artis on film.artis = artis.kd_artis
	JOIN negara on artis.negara = negara.kd_negara
	WHERE film.nm_film LIKE '___n%';

--9. Menampilkan nama artis yang tidak pernah bermain film									
SELECT artis.nm_artis FROM artis 
	LEFT JOIN film on film.artis = artis.kd_artis
	WHERE film.artis IS NULL;
SELECT artis.nm_artis FROM film 
	RIGHT JOIN artis on film.artis = artis.kd_artis
	WHERE film.artis IS NULL;

--10. Menampilkan nama artis yang bermain film dengan genre drama											
SELECT DISTINCT artis.nm_artis, genre.nm_genre FROM artis
	JOIN film on artis.kd_artis = film.artis
	JOIN genre on genre.kd_genre = film.genre
	WHERE genre.nm_genre = 'DRAMA';

--11. Menampilkan nama artis yang bermain film dengan genre Action											
SELECT DISTINCT artis.nm_artis, genre.nm_genre FROM artis
	JOIN film on artis.kd_artis = film.artis
	JOIN genre on genre.kd_genre = film.genre
	WHERE genre.nm_genre = 'ACTION';
SELECT artis.nm_artis, genre.nm_genre FROM artis
	JOIN film on artis.kd_artis = film.artis
	JOIN genre on genre.kd_genre = film.genre
	WHERE genre.nm_genre = 'ACTION'
	GROUP BY artis.nm_artis, genre.nm_genre;

--12. Menampilkan data negara dengan jumlah filmnya							
SELECT negara.kd_negara, negara.nm_negara, COUNT(film.id) as jumlah_film FROM film 
	JOIN artis on artis.kd_artis = film.artis
	RIGHT JOIN negara on negara.kd_negara = artis.negara
	GROUP BY negara.kd_negara, negara.nm_negara;

--13. Menampilkan nama film yang skala internasional									
SELECT film.nm_film FROM film
	JOIN produser on film.produser = produser.kd_produser
	WHERE produser.international = 'YA';
SELECT film.nm_film, produser.nm_produser FROM film
	JOIN produser on film.produser = produser.kd_produser
	--WHERE produser.international = 'YA' AND (produser.nm_produser IN ('MARVEL', 'HONGKONG CINEMA'));
	WHERE produser.international = 'YA' AND (produser.nm_produser = 'MARVEL' OR produser.nm_produser = 'HONGKONG CINEMA');

--14. Menampilkan jumlah film dari masing2 produser							
SELECT produser.nm_produser, COUNT(film.id) as jumlah_film FROM produser
	LEFT JOIN film on produser.kd_produser = film.produser
	WHERE produser.nm_produser = 'MARVEL'
	GROUP BY produser.nm_produser
	HAVING COUNT(film.id) >= 3;	
	--HAVING COUNT(film.id) >= 3 AND produser.nm_produser = 'MARVEL';