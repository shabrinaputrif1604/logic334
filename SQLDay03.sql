--store procedure dan function

--CREATE SP (Stored Procedure)
CREATE PROCEDURE sp_RetrieveMahasiswa
	--parameter di sini
as 
BEGIN 
	SELECT name, address, email, nilai FROM mahasiswa
END

--EXEC/RUN SP
EXEC sp_RetrieveMahasiswa

--EDIT/ALTER SP
ALTER PROCEDURE sp_RetrieveMahasiswa
	--parameter di sini
	@id INT, 
	@nama VARCHAR(50)
as 
BEGIN 
	SELECT name, address, email, nilai FROM mahasiswa
	WHERE id = @id AND name = @nama
END

--EXEC/RUN SP using parameter
EXEC [sp_RetrieveMahasiswa] 1, 'Haikal Limansah'

--FUNCTION
--CREATE FUNCTION
CREATE FUNCTION fn_total_mahasiswa
(
	@id INT
)
RETURNS INT
BEGIN
	DECLARE @hasil INT
	SELECT @hasil = COUNT(id) FROM mahasiswa WHERE id = @id
	return @hasil
END

--RUN FUNCTION
SELECT dbo.fn_total_mahasiswa(1)

--EDIT/ALTER FUNCTION
ALTER FUNCTION fn_total_mahasiswa
(
	@id INT,
	@nama VARCHAR(50)
)
RETURNS INT
BEGIN
	DECLARE @hasil INT
	SELECT @hasil = COUNT(id) FROM mahasiswa WHERE id = @id AND name = @nama
	return @hasil
END

--RUN FUNCTION
SELECT dbo.fn_total_mahasiswa(1, 'Haikal Limansah');

--SELECT FUNCTION MAHASISWA
SELECT mhs.id, mhs.name, dbo.fn_total_mahasiswa(mhs.id, mhs.name)
	FROM mahasiswa as mhs
	JOIN biodata as bio on bio.mahasiswa_id = mhs.id

--TRUNCATE TABLE(delete and reset table)
TRUNCATE TABLE [namatable]