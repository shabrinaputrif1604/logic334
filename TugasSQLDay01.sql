--TugasSQLDay01

--Buat database DBPenerbit
CREATE DATABASE DBPenerbit;
USE DBPenerbit;

CREATE TABLE tblPengarang 
(
	id INT PRIMARY KEY IDENTITY (1,1),
	kd_pengarang VARCHAR (7) NOT NULL,
	nama VARCHAR (30) NOT NULL,
	alamat VARCHAR (80) NOT NULL,
	kota VARCHAR (15) NOT NULL,
	kelamin VARCHAR (1) NOT NULL
)

INSERT INTO tblPengarang (kd_pengarang, nama, alamat, kota, kelamin) VALUES
	('P0001', 'Ashadi', 'Jl. Beo 25', 'Yogya', 'P'),
	('P0002', 'Rian', 'Jl. Solo 123', 'Yogya', 'P'),
	('P0003', 'Suwadi', 'Jl. Semangka 13', 'Bandung', 'P'),
	('P0004', 'Siti', 'Jl. Durian 15', 'Solo', 'W'),
	('P0005', 'Amir', 'Jl. Gajah 33', 'Kudus', 'P'),
	('P0006', 'Suparman', 'Jl. Harimau 25', 'Jakarta', 'P'),
	('P0007', 'Jaja', 'Jl. Singa 7', 'Bandung', 'P'),
	('P0008', 'Saman', 'Jl. Naga 12', 'Yogya', 'P'),
	('P0009', 'Anwar', 'Jl. Tidar 6A', 'Magelang', 'P'),
	('P0010', 'Fatmawati', 'Jl. Renjana 4', 'Bogor', 'W')

SELECT * FROM tblPengarang

CREATE TABLE tblGaji 
(
	id INT PRIMARY KEY IDENTITY (1,1),
	kd_pengarang VARCHAR (7) NOT NULL,
	nama VARCHAR (30) NOT NULL,
	gaji DECIMAL (18,4) NOT NULL
)

INSERT INTO tblGaji (kd_pengarang, nama, gaji) VALUES
	('P0002', 'Rian', 600000),
	('P0005', 'Amir', 700000),
	('P0004', 'Siti', 500000),
	('P0003', 'Suwadi', 1000000),
	('P0010', 'Fatmawati', 600000),
	('P0008', 'Saman', 750000);

SELECT * FROM tblGaji;

--1. Hitung dan tampilkan jumlah pengarang dari table tblPengarang.				
SELECT COUNT(id) as jumlah_pengarang FROM tblPengarang;

--2. Hitunglah berapa jumlah Pengarang Wanita dan Pria
SELECT COUNT(kelamin) as jml_pengarang, kelamin FROM tblPengarang GROUP BY kelamin;

--3. Tampilkan record kota dan jumlah kotanya dari table tblPengarang.
SELECT COUNT(kota) as jml_kota,kota FROM tblPengarang GROUP BY kota;
SELECT COUNT(kota) as jml_kota,kota FROM tblPengarang WHERE NOT kota = 'Magelang' AND NOT kota = 'Solo' GROUP BY kota;

--4. Tampilkan record kota diatas 1 kota dari table tblPengarang.				
SELECT COUNT(kota) as jml_kota,kota FROM tblPengarang GROUP BY kota HAVING COUNT(kota) > 1;

--5. Tampilkan Kd_Pengarang yang terbesar dan terkecil dari table tblPengarang.					
SELECT kd_pengarang, nama FROM tblPengarang ORDER BY kd_pengarang DESC;

--6. Tampilkan gaji tertinggi dan terendah.		
SELECT MAX(gaji) as gaji_max, MIN(gaji) as gaji_min FROM tblGaji;

--7. Tampilkan gaji diatas 600.000.		
SELECT gaji FROM tblGaji WHERE gaji > 600000;

--8. Tampilkan jumlah gaji.	
SELECT SUM(gaji) as jml_gaji FROM tblGaji;

--9. Tampilkan jumlah gaji berdasarkan Kota			
SELECT SUM(data_gaji.gaji) as jml_gaji, data_pengarang.kota
	FROM tblGaji as data_gaji 
	JOIN tblPengarang as data_pengarang 
	on data_gaji.kd_pengarang = data_pengarang.kd_pengarang
	GROUP BY kota;

--10. Tampilkan seluruh record pengarang antara P0003-P0006 dari tabel pengarang.					
SELECT * FROM tblPengarang WHERE kd_pengarang BETWEEN 'P0003' AND 'P0006';

--11. Tampilkan seluruh data yogya, solo, dan magelang dari tabel pengarang.				
SELECT * FROM tblPengarang WHERE kota = 'Yogya' OR kota = 'Solo' OR kota = 'Magelang';
SELECT * FROM tblPengarang WHERE kota IN ('Yogya', 'Solo', 'Magelang');

--12. Tampilkan seluruh data yang bukan yogya dari tabel pengarang.	
SELECT * FROM tblPengarang WHERE NOT kota = 'yogya';

--13. Tampilkan seluruh data pengarang yang nama (terpisah):					
--a. dimulai dengan huruf [A]
SELECT * FROM tblPengarang WHERE nama LIKE 'a%'
--b. berakhiran [i]	
SELECT * FROM tblPengarang WHERE nama LIKE '%i'
--c. huruf ketiganya [a]
SELECT * FROM tblPengarang WHERE nama LIKE '__a%'
--d. tidak berakhiran [n]
SELECT * FROM tblPengarang WHERE nama NOT LIKE '%n'

--14. Tampilkan seluruh data table tblPengarang dan tblGaji dengan Kd_Pengarang yang sama						
SELECT *
	FROM tblGaji as data_gaji 
	JOIN tblPengarang as data_pengarang 
	on data_gaji.kd_pengarang = data_pengarang.kd_pengarang

--15. Tampilan kota yang memiliki gaji dibawah 1.000.000			
SELECT kota, gaji FROM tblPengarang JOIN tblGaji 
	on tblPengarang.kd_pengarang = tblGaji.kd_pengarang 
	WHERE gaji < 1000000

--16. Ubah panjang dari tipe kelamin menjadi 10			
ALTER TABLE tblPengarang ALTER COLUMN
	kelamin VARCHAR(10) NOT NULL

--17. Tambahkan kolom [Gelar] dengan tipe Varchar (12) pada tabel tblPengarang				
ALTER TABLE tblPengarang ADD
	gelar VARCHAR(12) NULL;

--18. Ubah alamat dan kota dari Rian di table tblPengarang menjadi, Jl. Cendrawasih 65 dan Pekanbaru						
UPDATE tblPengarang SET alamat = 'Jl. Cendrawasih 65' WHERE nama = 'Rian';
UPDATE tblPengarang SET kota = 'Pekanbaru' WHERE nama = 'Rian';

--19. Buatlah view untuk attribute Kd_Pengarang, Nama, Kota, Gaji dengan nama vwPengarang					
CREATE VIEW vw_pengarang
	AS SELECT tblPengarang.kd_pengarang, tblPengarang.nama, tblPengarang.kota, tblGaji.gaji FROM tblPengarang 
	JOIN tblGaji on tblPengarang.kd_pengarang = tblGaji.kd_pengarang;

SELECT * FROM vw_pengarang;